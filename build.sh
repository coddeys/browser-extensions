#!/bin/bash
if ! [ -e ./dist ]
then
   mkdir dist
fi
cp webextension.html dist/index.html
cp webextension-anatomy.png dist/webextension-anatomy.png
